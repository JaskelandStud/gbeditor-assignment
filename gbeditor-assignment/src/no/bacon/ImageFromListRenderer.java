package no.bacon;

import java.awt.GridBagConstraints;

import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableCellRenderer;
/**
 * @author      HIG Studenter 121232 @ hig.no
 * @version     13.37
 * @since       2014-10-17
 * Extending DefaultTableCellRenderer only to override setValue and make sure it uses setIcon instead of setText.
 * It keeps an array of ImageIcons and checks on the description (which corresponds to the integer value they are a representation of) to determine which image to display.
 */
public class ImageFromListRenderer extends DefaultTableCellRenderer{

	ImageIcon[] images;
	
	public ImageFromListRenderer(ImageIcon[] items) {
		images = items;
	}
	
	@Override
	public void setValue(Object value){
		for (ImageIcon image : images) {
			if (image.toString().equals((String)value.toString())) {
				setIcon(image);
			}
		}
	}
}
