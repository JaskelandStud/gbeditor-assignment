package no.bacon;

/**
 * @author      HIG Studenter 121232 @ hig.no
 * @version     13.37
 * @since       2014-10-17
 * This class contains the data used to build the components in the generated GridbagLayout code.
 * Every row in the table corresponds to one GuiElement class.
 * The values are strings, booleans and ints because the table does not care about data types. This only matters at code generation, and even then the strings are easier than getting string names of classes.
 * There are only get and set functions.
 */

public class GuiElement {
	private String varName;
	private String text;
	private String component;
	private int row, column, rows, columns;
	private int fill, anchor;
	private int breadth, height;
	private Boolean useJScrollPane;
	private Boolean useWordWrap;
	
	public GuiElement(int id){
		component = "JLabel";
		varName = "var" + String.valueOf(id);
		text = "Text";
		row = 0;
		column = 0;
		rows = 0;
		columns = 0;
		fill = 0;
		anchor = 10;
		breadth = 0;
		height = 0;
		useJScrollPane = true;
		useWordWrap = true;
	}
	
	public int getBreadth() {
		return breadth;
	}

	public void setBreadth(int breadth) {
		this.breadth = breadth;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public Boolean getUseJScrollPane() {
		return useJScrollPane;
	}

	public void setUseJScrollPane(Boolean useJScrollPane) {
		this.useJScrollPane = useJScrollPane;
	}

	public Boolean getUseWordWrap() {
		return useWordWrap;
	}

	public void setUseWordWrap(Boolean useWordWrap) {
		this.useWordWrap = useWordWrap;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getColumns() {
		return columns;
	}

	public void setColumns(int columns) {
		this.columns = columns;
	}

	public int getAnchor() {
		return anchor;
	}

	public void setAnchor(int anchor) {
		this.anchor = anchor;
	}

	public int getFill() {
		return fill;
	}

	public void setFill(int fill) {
		this.fill = fill;
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getComponent() {
		return component;
	}

	public void setComponent(String component) {
		this.component = component;
	}
}

