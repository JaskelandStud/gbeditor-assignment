package no.bacon;
import java.awt.GridBagConstraints;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.TableColumn;

/**
 * Launches the main JFrame window which displays the program, and launches everything 
 * else.
 * <p>
 * @author      HIG Studenter <121232 @ hig.no>
 * @version     13.37
 * @since       2014-10-17
 * @param currentVersion holds the version of the application. Must be edited whenever 
 * there is a change in the saving and loading system so that the program knows which
 * files it can load.
 * @param table is the table which all the contents of tableModel is displayed.
 * @param tableModel holds the logical model of the table and its attributes.
 * @param componentTypeName holds the strings of the types of components which can be 
 * chosen from the type dropdown list in the table.
 */

@SuppressWarnings("serial")
public class GBEditor extends JFrame{
	private JTable table;
	private TableModel tableModel;
	private String componentTypeName[] = {"JLabel","JTextField","JTextArea","JButton"};
	String currentVersion = "GBE Version 1337";
	
	public GBEditor(ResourceBundle messages){
		super(messages.getString("programTitle"));
		TableModel tableModel = new TableModel(messages);
		table = new JTable(tableModel);
		table.getColumnModel().getColumn(0).setMinWidth(50);
		table.getColumnModel().getColumn(1).setMinWidth(100);
		table.getColumnModel().getColumn(2).setMinWidth(50);
		table.getColumnModel().getColumn(3).setMinWidth(50);
		table.getColumnModel().getColumn(4).setMinWidth(50);
		table.getColumnModel().getColumn(5).setMinWidth(50);
		table.getColumnModel().getColumn(6).setMinWidth(60);
		table.getColumnModel().getColumn(7).setMinWidth(50);
		table.getColumnModel().getColumn(7).setPreferredWidth(100);
		table.getColumnModel().getColumn(8).setMinWidth(50);
		table.getColumnModel().getColumn(8).setPreferredWidth(100);
		
		ImageIcon[] fillImages = {
			new ImageIcon(getClass().getResource("skaler_ingen.png"), Integer.toString(GridBagConstraints.NONE)),
			new ImageIcon(getClass().getResource("skaler_horisontalt.png"), Integer.toString(GridBagConstraints.HORIZONTAL)),
			new ImageIcon(getClass().getResource("skaler_vertikalt.png"), Integer.toString(GridBagConstraints.VERTICAL)),
			new ImageIcon(getClass().getResource("skaler_begge.png"), Integer.toString(GridBagConstraints.BOTH))
		};
		
		ImageIcon[] anchorImages = {
			new ImageIcon(getClass().getResource("anchor_center.png"), Integer.toString(GridBagConstraints.CENTER)),
			new ImageIcon(getClass().getResource("anchor_north.png"), Integer.toString(GridBagConstraints.NORTH)),
			new ImageIcon(getClass().getResource("anchor_south.png"), Integer.toString(GridBagConstraints.SOUTH)),
			new ImageIcon(getClass().getResource("anchor_east.png"), Integer.toString(GridBagConstraints.EAST)),
			new ImageIcon(getClass().getResource("anchor_west.png"), Integer.toString(GridBagConstraints.WEST)),
			new ImageIcon(getClass().getResource("anchor_northeast.png"), Integer.toString(GridBagConstraints.NORTHEAST)),
			new ImageIcon(getClass().getResource("anchor_northwest.png"), Integer.toString(GridBagConstraints.NORTHWEST)),
			new ImageIcon(getClass().getResource("anchor_southeast.png"), Integer.toString(GridBagConstraints.SOUTHEAST)),
			new ImageIcon(getClass().getResource("anchor_southwest.png"), Integer.toString(GridBagConstraints.SOUTHWEST)),
			
		};
		
		JComboBox<String> componentTypeSelector = new JComboBox<String>(componentTypeName);
		JComboBox<ImageIcon> fillSelector = new JComboBox<ImageIcon>(fillImages);
		JComboBox<ImageIcon> anchorSelector = new JComboBox<ImageIcon>(anchorImages);
		
		TableColumn typeColumn = table.getColumnModel().getColumn(0);
		TableColumn fillColumn = table.getColumnModel().getColumn(7);
		TableColumn anchorColumn = table.getColumnModel().getColumn(8);
		
		
		typeColumn.setCellEditor(new DefaultCellEditor(componentTypeSelector));
		fillColumn.setCellEditor(new DefaultCellEditor(fillSelector));
		anchorColumn.setCellEditor(new DefaultCellEditor(anchorSelector));
		
		fillColumn.setCellRenderer(new ImageFromListRenderer(fillImages));
		anchorColumn.setCellRenderer(new ImageFromListRenderer(anchorImages));
		
		MenuHandler menuHandler = new MenuHandler(currentVersion, this, table, messages);
		menuHandler.tableModel = tableModel;
		
		getContentPane().add(new JScrollPane(table));
		tableModel.addRowEmpty();
		super.setSize(800, 300);
		
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		
	}
	
	//== I AM MAIN AND I LOVE YOU ==
	public static void main(String args[]){
		//Internationalization bit
		String language = "en";
		String country = "US";
		
		if(args.length == 2){
			language = new String(args[0]);
			country = new String(args[1]);
		}
		
		Locale currentLocale = new Locale(language, country);
		ResourceBundle messages = ResourceBundle.getBundle("no.bacon.MessageBundle", currentLocale);
		
		
		try {
            // Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    } catch (UnsupportedLookAndFeelException e) {
	       // handle exception
	    } catch (ClassNotFoundException e) {
	       // handle exception
	    } catch (InstantiationException e) {
	       // handle exception
	    } catch (IllegalAccessException e) {
	       // handle exception
	    }
			@SuppressWarnings("unused")
			GBEditor editor = new GBEditor(messages);
		}
	
	
	public static ImageIcon createImageIcon(String path) {
        
    	URL imageURL = GBEditor.class.getResource(path);
    	
        if (imageURL != null) {
            return new ImageIcon(imageURL);
        } else {
            return null;
        }
    }
}
