package no.bacon;

import java.util.ResourceBundle;
import java.util.Vector;

import javax.swing.table.AbstractTableModel;

/**
 * @author      HIG Studenter <121232 @ hig.no>
 * @version     13.37
 * @since       2014-10-17
 * Custom JTable model that extends AbstractTableModel. Stores the data for every row as a GuiElement class in the vector data.
 * Increments an integer "id" for every new row to give new entries unique variable name identifiers as a convienience for the user.
 * Hardcoded to have 9 defined columns.
 */

@SuppressWarnings("serial")
public class TableModel extends AbstractTableModel{
	Vector<GuiElement> data = new Vector<GuiElement>();
	int uniqueId = 0;
	private String columNames[] = new String[9];
	
	public TableModel(ResourceBundle messages) {
		super();
		for(int i = 0; i < 9; i++){
			String columnName = "column" + i + "Name";
			columNames[i] = messages.getString(columnName);
		}
	}
	
	/**
	 * Adds a new row to the table and fire an even to redraw the table.
	 */
	public void addRowEmpty(){
		data.add(new GuiElement(uniqueId));
		uniqueId++;
		fireTableRowsInserted (data.size()-1, data.size()-1);
	}
	
	/**
	 * Clears the table by removing all rows, and resetting the variable id. Fires an event to redraw the table.
	 */
	public void resetRows(){
		data.clear();
		uniqueId = 0;
		fireTableRowsInserted (data.size()-1, data.size()-1);
	}
	
	public void resetUniqueId(){

		uniqueId = 0;

	}
	
	@Override
	public int getColumnCount() {
		return columNames.length;
	}

	@Override
	public int getRowCount() {
		return data.size();
	}

	@Override
	public String getColumnName(int index){
		return columNames[index];
	}
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return true;
	}
	
	@Override
	public Object getValueAt(int row, int col) {
		switch(col){
		case 0 : return data.elementAt(row).getComponent();
		case 1 : return data.elementAt(row).getVarName();
		case 2 : return data.elementAt(row).getText();
		case 3 : return data.elementAt(row).getRow();
		case 4 : return data.elementAt(row).getColumn();
		case 5 : return data.elementAt(row).getRows();
		case 6 : return data.elementAt(row).getColumns();
		case 7 : return data.elementAt(row).getFill();
		case 8 : return data.elementAt(row).getAnchor();
		}
		return null;
	}
	
	@Override
	public void setValueAt(Object value, int row, int col){
		if(value != null){
		switch(col){
			case 0 : data.elementAt(row).setComponent((String)value);
				break;
			case 1 : data.elementAt(row).setVarName((String)value);
				break;
			case 2 : data.elementAt(row).setText((String)value);
				break;
			case 3 : data.elementAt(row).setRow(Integer.parseInt((String)value));
				break;
			case 4 : data.elementAt(row).setColumn(Integer.parseInt((String)value));
				break;
			case 5 : data.elementAt(row).setRows(Integer.parseInt((String)value));
				break;
			case 6 : data.elementAt(row).setColumns(Integer.parseInt((String)value));
				break;
			case 7 : data.elementAt(row).setFill(Integer.parseInt((String)value.toString()));
				break;
			case 8 : data.elementAt(row).setAnchor(Integer.parseInt((String)value.toString()));
				break;
			}
		}
	}
	
	/**
	 * Removes one row in the model and fires an event to redraw the table
	 * @param index 	The index of the row to remove
	 */
	public void removeRow(int index) {
		data.remove(index);
		fireTableRowsInserted (data.size()-1, data.size()-1);
	}


}
